package com.usecasenew.usecasenew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsecasenewApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsecasenewApplication.class, args);
	}

}
